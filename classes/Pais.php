<?php
declare(strict_types=1);

class Pais{
    private $iso;
    private $nombre;
    private $prefijo;

    private $gastos;

    public function __construct($iso, $nombre, $prefijo){
        $this->iso = $iso;
        $this->nombre = $nombre;
        $this->prefijo = $prefijo;

        $this->gastos = [
            'es' => 12.0,
            'uk' => 15.0,
        ];
    }

    public function calcularEnvio(): float{
        $ret = 0.0;

        if(isset($this->gastos[$this->iso])){
            $ret = $this->gastos[$this->iso];
        }

        return $ret;
    }

    public function listar(): Array{
        return [
            'iso' => $this->iso,
            'nombre' => $this->nombre,
            'prefijo' => $this->prefijo,
        ];
    }
}