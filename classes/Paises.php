<?php
declare(strict_types=1);

require __DIR__ . '/Pais.php';

/*
 * Clase que obtiene la información de los paises
 */
class Paises{
    private $paises;

    /**
     * Conectará con la base de datos para obtener el listado de paises
     */
    public function __construct(){
        $paises = [
            [
                'iso' => 'es',
                'nombre' => 'España',
                'prefijo' => 34,
            ], [
                'iso' => 'uk',
                'nombre' => 'Reino Unido',
                'prefijo' => 44,
            ],
        ];

        foreach($paises as $pais){
            $this->paises[] = new Pais($pais['iso'], $pais['nombre'], $pais['prefijo']);
        }
    }

    /**
     * Devuelve el listado de operaciones disponibles para esta clase.
     */
    public function operacionesDisponibles(){
        return $operaciones = [
            'listar' => 'listar',
            'nuevo'  => 'nuevo',
        ];
    }

    /**
     * Devuelve el listado de paises disponibles.
     */
    public function listar(){
        $toRet = [
            'template' => 'paises/listar',
            'datos' => [
                'paises' => [],
            ],
        ];

        foreach($this->paises as $pais){
            $toRet['datos']['paises'][] = $pais->listar();
        }

        return $toRet;
    }
}