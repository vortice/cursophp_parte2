<?php
require_once __DIR__ . '/vendor/autoload.php';

$loader = new Twig_Loader_Filesystem(__DIR__ . '/templates');
$twig = new Twig_Environment($loader);

/**
 * Definición de las secciones disponibles.
 */
$secciones = [
  'paises'  => 'Paises',
];

$operacion = '';

// Plantilla por defecto
$nombrePlantilla = 'index.html.twig';
// Sin datos
$datos = [];

// Se precisa siempre una sección. En caso contrario muestra el inicio
if(isset($_GET['s']) && isset($secciones[$_GET['s']])){
    // Creamos un objeto a partir de la Clase indicada.
    require __DIR__ . '/classes/' . $secciones[$_GET['s']] . '.php';
    $seccion = new $secciones[$_GET['s']];

    $operaciones = $seccion->operacionesDisponibles();
    if(isset($_GET['op']) && isset($operaciones[$_GET['op']])){
        // Obtenemos el nombre del método que realiza la operación
        $metodo = $operaciones[$_GET['op']];

        // Llamamos al método de la operación seleccionada
        $infoOperacion = $seccion->$metodo();
        $nombrePlantilla = $infoOperacion['template'] . '.html.twig';
        $datos = $infoOperacion['datos'];
    }
}

// Mostramos la plantilla seleccionada
echo $twig->render($nombrePlantilla, $datos);